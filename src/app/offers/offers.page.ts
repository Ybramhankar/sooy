import { Component, OnInit } from '@angular/core';
import { ViewChild} from '@angular/core';
import { NavController } from '@ionic/angular';
import { Router } from '@angular/router';


@Component({
  selector: 'app-offers',
  templateUrl: './offers.page.html',
  styleUrls: ['./offers.page.scss'],
})
export class OffersPage implements OnInit {

  

  ngOnInit() {
  }


public images: any;
  @ViewChild('slider') slider ;
  page = 0;
  constructor(public navCtrl: NavController, private router: Router) {
  }

  selectedTab(index) {
    this.slider.slideTo(index);
  }




}
