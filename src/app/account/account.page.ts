import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-account',
  templateUrl: './account.page.html',
  styleUrls: ['./account.page.scss'],
})
export class AccountPage implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }



liveChat(){
	this.router.navigate(['live-chat']);
}

offers(){
	this.router.navigate(['offers']);
}

notifications(){
	this.router.navigate(['notifications']);
}

address(){
	this.router.navigate(['my-address']);
}

terms(){
	this.router.navigate(['terms']);
}

privacyPolicy(){
	this.router.navigate(['privacy-policy']);
}

faq(){
	this.router.navigate(['faq']);
}

favourites(){
	this.router.navigate(['my-favourites']);
}


feedback(){
	this.router.navigate(['feedback']);
}





}
