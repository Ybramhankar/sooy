import { Component, OnInit } from '@angular/core';
import { ViewChild} from '@angular/core';
import { NavController } from '@ionic/angular';
import { Router } from '@angular/router';

@Component({
  selector: 'app-hotel',
  templateUrl: './hotel.page.html',
  styleUrls: ['./hotel.page.scss'],
})
export class HotelPage implements OnInit {

	// @ViewChild('SwipedTabsSlider') SwipedTabsSlider ;

	// SwipedTabsIndicator :any= null;
 //  	tabs:any=[];

 //  constructor(public navCtrl: NavController) {
 //  	this.tabs=["page1","page2","page3","page4"];
 //  }
  // ionViewDidEnter() {
  //   this.SwipedTabsIndicator = document.getElementById("indicator");
  // }

  // selectTab(index) {    
  //   this.SwipedTabsIndicator.style.webkitTransform = 'translate3d('+(100*index)+'%,0,0)';
  //   this.SwipedTabsSlider.slideTo(index, 500);
  // }

  // updateIndicatorPosition() {
  //     // this condition is to avoid passing to incorrect index
  // 	if( this.SwipedTabsSlider.length()> this.SwipedTabsSlider.getActiveIndex())
  // 	{
  // 		this.SwipedTabsIndicator.style.webkitTransform = 'translate3d('+(this.SwipedTabsSlider.getActiveIndex() * 100)+'%,0,0)';
  // 	}
    
  //   }

  // animateIndicator($event) {
  // 	if(this.SwipedTabsIndicator)
  //  	    this.SwipedTabsIndicator.style.webkitTransform = 'translate3d(' + (($event.progress* (this.SwipedTabsSlider.length()-1))*100) + '%,0,0)';
  // }




public images: any;
  @ViewChild('slider') slider ;
  page = 0;
  constructor(public navCtrl: NavController, private router: Router) {
  }

  selectedTab(index) {
    this.slider.slideTo(index);
  }



OpenItemDdetails(){
  this.router.navigate(['item-details'])
}


















  ngOnInit() {
  }

}
