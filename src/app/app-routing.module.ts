import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: '', loadChildren: './tabs/tabs.module#TabsPageModule' },
  { path: 'account', loadChildren: './account/account.module#AccountPageModule' },
  { path: 'location', loadChildren: './location/location.module#LocationPageModule' },
  { path: 'menu', loadChildren: './menu/menu.module#MenuPageModule' },
  { path: 'hotel', loadChildren: './hotel/hotel.module#HotelPageModule' },
  { path: 'item-details', loadChildren: './item-details/item-details.module#ItemDetailsPageModule' },
  { path: 'hotel-info', loadChildren: './hotel-info/hotel-info.module#HotelInfoPageModule' },
  { path: 'live-chat', loadChildren: './live-chat/live-chat.module#LiveChatPageModule' },
  { path: 'offers', loadChildren: './offers/offers.module#OffersPageModule' },
  { path: 'notifications', loadChildren: './notifications/notifications.module#NotificationsPageModule' },
  { path: 'my-address', loadChildren: './my-address/my-address.module#MyAddressPageModule' },
  { path: 'terms', loadChildren: './terms/terms.module#TermsPageModule' },
  { path: 'privacy-policy', loadChildren: './privacy-policy/privacy-policy.module#PrivacyPolicyPageModule' },
  { path: 'faq', loadChildren: './faq/faq.module#FaqPageModule' },
  { path: 'my-favourites', loadChildren: './my-favourites/my-favourites.module#MyFavouritesPageModule' },
  { path: 'feedback', loadChildren: './feedback/feedback.module#FeedbackPageModule' }
];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
