import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MyFavouritesPage } from './my-favourites.page';

describe('MyFavouritesPage', () => {
  let component: MyFavouritesPage;
  let fixture: ComponentFixture<MyFavouritesPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MyFavouritesPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MyFavouritesPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
