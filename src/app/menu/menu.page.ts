import { Component, OnInit } from '@angular/core';
import { ViewChild } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.page.html',
  styleUrls: ['./menu.page.scss'],
})
export class MenuPage implements OnInit {


	@ViewChild('mySlider')  slides ;


  constructor(private router: Router){


  	
  }
		hotelPage(){
			this.router.navigate(['hotel'])
		};

  ngOnInit() {

  }

slidesDidLoad() {
	this.slides.startAutoplay();
}

hotelInfo(){
  this.router.navigate(['hotel-info']);
}


}
