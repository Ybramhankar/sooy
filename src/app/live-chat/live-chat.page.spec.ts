import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LiveChatPage } from './live-chat.page';

describe('LiveChatPage', () => {
  let component: LiveChatPage;
  let fixture: ComponentFixture<LiveChatPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LiveChatPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LiveChatPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
